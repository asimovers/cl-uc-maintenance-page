import InformationBox from "./components/InformationBox.vue";
import InformationBoxEn from "./components/InformationBoxEn.vue";

import { createApp } from "vue/dist/vue.esm-bundler.js";

const app = createApp({});

app.component("InformationBox", InformationBox);
app.component("InformationBoxEn", InformationBoxEn);
app.mount("#app");
